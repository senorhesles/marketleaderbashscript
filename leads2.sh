#!/bin/bash
# JMJ

global_var=1

s00 ()
{
    sleep $(bc -l <<< "$global_var/100")
}

s0 ()
{
    sleep $(bc -l <<< "$global_var/10")
}

s03 ()
{
    sleep $(bc -l <<< "$global_var/3")
}


s05 ()
{
    
    sleep $(bc <<< "scale=2; $global_var/2")
}

s1 ()
{
    sleep $global_var
}

s2 ()
{
    sleep $(bc <<< "scale=2; $global_var*2")
}

s3 ()
{
    sleep $(bc <<< "scale=2; $global_var*3")
}

s5 ()
{
    sleep $(bc <<< "scale=2; $global_var*5")
}

s10 ()
{
    sleep $(bc <<< "scale=2; $global_var*10")
}

s0; google-chrome http://www.mysanantoniotexashome.com/adminlogin/; s5

# verify page has loaded and write to temporary file

until echo "$ad" | grep Admin ; do
    s05; echo "Page hasn't loaded yet...waiting"
    xdotool mousemove 1 440; s0; xdotool click 1
    xdotool key ctrl+a; s0;
    xdotool key ctrl+c; s0;
    ad="$(clipit -c)"; s0;
done

# Click 'Login' button and login
s2
echo "Read 'Admin'; page loaded; proceeding to dashboard"
xdotool key ctrl+f; s0
xdotool type --delay 40 "Mario"; s0
xdotool key Escape; s0
xdotool key Tab; s0
xdotool key Tab; s0
xdotool key Return; s1

# We are now waiting for the Dashboard page to finish loading

until echo "$looking" | grep Looking; do # we need to update the number every new year..so 2016..change it to 16..could add
    s05; echo "Page hasn't loaded yet...waiting"
    xdotool key ctrl+a; s0
    xdotool key ctrl+c; s0
    looking="$(clipit -c)"
    xdotool mousemove 2518 333; s0
    xdotool click 1; s1
done

xdotool click 1; s2 # clear modifiers
# We are ready to start working on the leads

echo "Read 'Looking in'; page loaded; proceeding to find newest lead"
xdotool key ctrl+f; s05
xdotool type 'View'; s0
xdotool key Escape; s0
xdotool key Tab; s0
xdotool key Return; s0
xdotool click 1; s3 # clear modifiers

# We have just accessed the newest lead and need to verify the page is loaded
until echo "$prim" | grep Primary; do
    echo "Lead page hasn't loaded yet...waiting"
    xdotool key ctrl+f; s0
    xdotool type --delay 50 "Primary"; s0
    xdotool key Escape; s0
    xdotool key ctrl+c; s0
    prim="$(clipit -c)"
done

# The Lead page has been loaded
echo "Read 'Primary Details'; page loaded; proceeding"

# Name Correction Function
s1
xdotool key ctrl+f; s0 
xdotool type --delay 40 'Send Listings'; s03
xdotool key Escape; s03
xdotool key Tab; s03
xdotool key Return; s03

echo "Attempting to edit leads name"

xdotool key ctrl+f; s03
xdotool type 'Cancel'; s03
xdotool key Escape; s03

# clear modifiers
echo "H" | xsel -b


# Script proceeds to go to the first name
xdotool key Tab; s0
xdotool key Tab; s0

# Script copies first name
xdotool key ctrl+c; s0

# Script uncapitalizes all, then capitalizes first letter
first="$(clipit -c)"; first=($first); first=${first,,}; first=${first[@]^}; xdotool type --delay 30 $first
echo "Contact's first name should have a first letter that is upper case"

# Script proceeds to go to last name
xdotool key Tab; s0

# Capitalization sequence
xdotool key ctrl+c; s0

# Script uncapitalizes all, then capitalizes first letter
last="$(clipit -c)"; last=($last); last=${last,,}; last=${last[@]^}; xdotool type --delay 30 $last; s0
echo "Contact's last name should have a first letter that is upper case"

# We should have a part of the script that makes sure no content in any of the boxes is fucked
# Script proceeds to Click 'save'
echo "Saving updated name"; s05

# I'm gonna try a bit to get rid of the stupid html in the comments

xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key Tab; s0; xdotool key Tab; s0;
xdotool key ctrl+a; s03; xdotool key ctrl+c; s03;
comments="$(clipit -c)"; revisedcomments="$(echo $comments | sed 's/[^a-z/ [A-Z]]*//g')"; xdotool type --delay 50 "$revisedcomments"

xdotool key ctrl+f; s03
xdotool type --delay 50 "Save"; s03
xdotool key Escape; s03
xdotool key Return; s5

xdotool key ctrl+f; s1
xdotool type --delay 50 "Summary"; s1
xdotool key Escape; s1
xdotool key Return; s5

# PAGE ORIENTATION SCRIPT
# this part scrolls the page until it gets to the line with "Alerts" in it
alerts="foo"; activewin="$(xdotool getactivewindow)"
xdotool mousemove --window "$activewin" 10 225
until echo "$alerts" | grep Alert; do
    s00
    xdotool mousemove_relative --polar --sync 180 5; xdotool mousedown 1; s03
    xdotool mousemove_relative --polar --sync 90 245; xdotool mouseup 1; s03
    xdotool key ctrl+c
    alerts="$(clipit -c)"; echo $alerts
    xdotool mousemove_relative --polar 270 245; s0
    xdotool click 1; s00
done


# MOUSE IS ORIENTED


eval $(xdotool getmouselocation --shell) # this saves the x y variables for when we are doing stuff later
coords="$(echo $X $Y)"
# this is the testing part where I try and capture the Viewed (0) number
xdotool mousemove_relative --polar 90 540; s0
xdotool mousedown 1; s0
xdotool mousemove_relative --polar 270 210; s0
xdotool mouseup 1; s0
xdotool key ctrl+c; s0
viewed="$(clipit -c)"; echo $viewed
numberviewed="$(echo "$viewed" | sed 's/[^0-9]*//g')"; echo $numberviewed
numberalerts="$(echo "$alerts" | sed 's/[^0-9]*//g')"; echo $numberalerts

# Go back to our saved location and determine where more actions is
xdotool mousemove $coords
xdotool mousemove_relative --polar 90 2000
xdotool mousemove_relative --polar 270 100
echo "this should be right above moreactions mouse should be"
xdotool mousemove_relative --polar 180 50
eval $(xdotool getmouselocation --shell)
morecoords="$(echo $X $Y)"

# BEGIN LISTING ALERT MODULE

# Begin test module that only actives if they've saved at most 0 alerts, but they have viewed at least 1

if [[ $numberviewed -eq 0 ]]
then
    echo "I haven't looked at anything on my own"; s0
    xdotool mousemove $morecoords; s05
    xdotool click 1; s05
    xdotool key ctrl+f; s05
    xdotool type --delay 40 'New List'; s05
    xdotool key Escape; s05
    xdotool key Return; s1
    
    # listing alert procedure
    xdotool key --delay 12 ctrl+f; s0
    xdotool type --delay 20 'freq'; s0
    xdotool key --delay 12 Escape; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 20 b; s0
    xdotool key --delay 12 Tab; s03
    xdotool mousemove 116 310; s03 # this is the location of the zip code field
    xdotool click 1
    for i in 78209 78216 78231 78232 78247 78248 78249 78253 78256 78257 78258 78259 78260 78261 ; do
	s03
	xdotool type --delay 50 $i; s1
	xdotool key Return; s03
	xdotool key ctrl+a; s00; xdotool key ctrl+c; s00
	ugg=$(clipit -c); t=1
	while echo $ugg | grep Suggest; do # this part is a recovery feature...sometimes the script will go too fast, it makes sure the zipcode is entered
	        s1
		xdotool key Shift+Tab; s1
	        xdotool key Return; s1 # get rid of pop up
		xdotool type --delay 20 'freq'; s0
		xdotool key --delay 12 Escape; s0
		xdotool key --delay 12 Tab; s03
		xdotool key --delay 12 Tab; s03
		xdotool key --delay 12 Tab; s03
		xdotool mousemove 116 310; s0
		xdotool click 1; s0
	        xdotool type --delay 500 $i; s1
	        xdotool click 1; s1
	        xdotool key Return; s1
	        xdotool key ctrl+a; s0; xdotool key ctrl+c
	        ugg=$(clipit -c)
	        done
        xdotool type --delay 20 'freq'; s0
	xdotool key --delay 12 Escape; s0
	xdotool key --delay 12 Tab; s0
	xdotool key --delay 12 Tab; s0
	xdotool key --delay 12 Tab; s0
	done
    xdotool mousemove 496 226; s0
    xdotool click 1; s0
    xdotool type --delay 20 '225000'; s0
    xdotool key --delay 12 Tab; s0
    xdotool type --delay 20 '500000'; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 20 3; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 20 2; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 12 ctrl+f; s0
    xdotool type --delay 50 "Add alert"; s0
    xdotool key --delay 12 Escape; s0
    xdotool key --delay 12 Return; s5
    xdotool mousemove $morecoords; s0
    xdotool click 1; s0
else
    s00; echo "We've started the I looked at some properties branch"
    xdotool key ctrl+f; s1
    xdotool type --delay 20 'Saved'; s1
    xdotool key Escape; s1
    xdotool key Return; s5   # by now it should have gotten to the saved listings paged
    xdotool key ctrl+a; s05
    xdotool key ctrl+c; s1
    xdotool mousemove 1 400; xdotool click 1; s00
    bigtext="$(clipit -c)"; reduxtext="$(echo "$bigtext" | grep ^[[:digit:]])"
    trimtext="$(echo "$reduxtext" | tr -d ',')"
    zips="$(echo "$trimtext" | egrep -o "\ 78[0-9]{3}" | sort | uniq)"
    prices="$(echo "$trimtext" | egrep -o '\$[0-9]*')"
    priceslowfirst="$(echo "$prices" | sed 's/[^0-9]*//g' | sort -n)"
    priceshighfirst="$(echo "$priceslowfirst" | sort -n -r)"
    firstlowprice="$(echo $priceslowfirst | head -n1 | sed -e 's/\s.*$//')"
    firsthighprice="$(echo $priceshighfirst | head -n1 | sed -e 's/\s.*$//')"
    bedrooms="$(echo "$bigtext" | cut -d $'\t' -f 5)"
    threebedcount="$(echo "$bedrooms" | grep '3' | wc -l)"
    fourbedcount="$(echo "$bedrooms" | grep '4' | wc -l)"
    fivebedcount="$(echo "$bedrooms" | grep '5' | wc -l)"
    xdotool key ctrl+f; s0
    xdotool type --delay 50 'Summa'; s0
    xdotool key Escape; s0
    xdotool key Return; s2
    
    # We are about to create the listing alert
    xdotool mousemove $morecoords; s03
    xdotool click 1; s03
    xdotool key ctrl+f; s03
    xdotool type 'New List'; s03
    xdotool key Escape; s03
    xdotool key Return; s3
    xdotool key --delay 12 ctrl+f; s1
    xdotool type --delay 20 'Area'; s0
    xdotool key --delay 12 Escape; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 12 Tab; s0
    xdotool key --delay 20 b; s0
    xdotool key --delay 12 Tab; s0
    xdotool mousemove 116 310; s0 # this is the location of the zip code field
    xdotool click 1
    for i in $zips; do
	xdotool type --delay 20 $i; s1
	xdotool key --delay 12 Return; s1
	xdotool key --delay 12 ctrl+a; s1
	xdotool key --delay 20 ctrl+c; s0
	errors="$(clipit -c)"
	t=1
	while echo $errors | grep -Fq 'uggested'; do
	    s1; xdotool key Shift+Tab; s1
	    xdotool key Return; s1
	    xdotool mousemove 116 310; s1
	    xdotool click 1; s1
	    echo "this is where I typed the zipcode again really slowly"
	    xdotool type --delay 500 $i; s1
	    xdotool click 1; s3
	    xdotool key Return; s1
	    xdotool key --delay 20 ctrl+a; s00
	    xdotool key --delay 20 ctrl+c; s00
	    errors="$(clipit -c)"
	done
        xdotool mousemove 116 310; s0
	xdotool click 1; s0
    done
    s1; xdotool mousemove 503 228; s1
    xdotool click 1; s1
    xdotool type --delay 20 $firstlowprice
    echo "just typed in the lowest priced listing"; s1
    xdotool key --delay 12 Tab; s1
    xdotool type --delay 20 $firsthighprice
    echo "just typed in the highest priced listing"; s1
    xdotool key --delay 12 Tab; echo "just pressed tab to go the bedroom"; s1
    if [ "$threebedcount" -gt "$fourbedcount" ] && [ "$threebedcount" -gt "$fivebedcount" ];
    then
	xdotool key 3
    else
	if [ "$fourbedcount" -gt "$fivebedcount" ];
	then
	    xdotool key 4
	else
	    if [ "$fivebedcount" -gt 0 ];
	    then
		xdotool key 5
	    else
		xdotool key 3
	    fi
	fi
    fi
    
    s1; xdotool key --delay 12 Tab; s1
    xdotool key --delay 20 2; s1
    xdotool key --delay 12 Tab; s1
    xdotool key --delay 12 ctrl+f; s0
    xdotool type --delay 50 "Add alert"; s0
    xdotool key --delay 12 Escape; s0
    xdotool key --delay 12 Return; s5
    xdotool key ctrl+f; s0
    xdotool type 'Summary'; s0
    xdotool key Escape; s0
    xdotool key Return; s3
fi

# new campaigns part

s3; xdotool mousemove $morecoords; s05 #should be going to right on top of More Actions
xdotool click 1; s05
xdotool key ctrl+f; s1
xdotool type --delay 20 'Add to Campa'; s1
xdotool key Escape; s1
xdotool key Return; s3

s0; xdotool key ctrl+f; s05; echo "trying to find month"
xdotool type --delay 50 'Month'; s05
xdotool key Escape; s05
xdotool key ctrl+c; s05; echo "trying to copy month"
month="$(clipit -c)"

if echo $month | grep -Fq 'Month'
then
    echo "I found month!"
    s0; xdotool key ctrl+f; s05
    xdotool type --delay 50 'Month'; s05
    xdotool key Escape; s05
    xdotool key Tab; s05
    xdotool key Tab; s05
    xdotool key Return; s2; echo "trying to click the add return button!"
fi



xdotool key ctrl+f; s1; echo "pressing ctrl+f"
xdotool type --delay 50 'Pain'; s05; echo "looking for pain"
xdotool key Escape; s05; echo "pressing Escape"
xdotool key ctrl+c; s05; echo "copying pain"
pain="$(clipit -c)"; echo $pain; echo "just echoed the thing I copied"
if echo $pain | grep -Fq 'Pain'
then
    echo "I found Pain!"
    s0; xdotool key ctrl+f; s05; echo "Trying to find the close button"
    xdotool type --delay 50 'Pain'; s05
    xdotool key Escape; s05
    xdotool key Tab; s05
    xdotool key Tab; s05
    xdotool key Return; s2; echo "I should have just tried to click Pain"
fi

xdotool key Escape; s05


xdotool key ctrl+f; s0
xdotool type --delay 20 'Summary'; s0
xdotool key Escape; s1
xdotool key Return; s2

xdotool mousemove $morecoords; s2 #should be going to right on top of More Actions
xdotool mousemove_relative --polar 270 500; s0 #this is supposed to move it to the leads part
xdotool click 1; s0
xdotool mousemove_relative --polar 180 130; s0 #this is supposed to move it to the Retry
xdotool click 1; s0 #commented out so they don't actually change it to retry
echo "I should have just changed him to retry right now...about to start campaign"; s1
xdotool key ctrl+f; s0
xdotool type --delay 20 'Summary'; s0
xdotool key Escape; s1
xdotool key Return; s2
xdotool key ctrl+f; s0 #Trying to find Send Email
xdotool type --delay 20 'Send Email'; s0
xdotool key Escape; s1
xdotool key Return; s2

until echo $bjec | grep -Fq "bjec"; do
    s1; xdotool mousemove 198 488; s00
    xdotool mousedown 1; s00
    xdotool mousemove 255 490; s00
    xdotool mouseup 1; s00
    xdotool key --delay 12 ctrl+c; s00
    xdotool click 1; s00
    bjec="$(clipit -c)"
done

s1; xdotool key ctrl+f; s0 #Here is where it tries to find the Subject Line to begin typing
xdotool type --delay 20 'Today'; s0
xdotool key Escape; s05
xdotool key Tab; s05
xdotool key Tab; s05
xdotool key Tab; s05
xdotool type --delay 20 'Quick question'; s0 # it should have found the Subject line and typed 'Quick Question at this point


s0; xdotool key Tab; s1
xdotool type --delay 20 'Hi '; s1
xdotool type --delay 20 $first; s1
xdotool type --delay 12 ','; s1
xdotool key shift+Return; s1
xdotool key shift+Return; s1
xdotool type --delay 20 'I see that you registered on my site, www.MySanAntonioTexasHome.com. Do you live locally or are you thinking of moving here?'; s1 #get data for city as variab
xdotool key shift+Return; s1
xdotool key shift+Return; s1
xdotool type --delay 20 'Best,'; s1
xdotool key shift+Return; s1
xdotool type --delay 20 'Mario Hesles'; s1; # it should have typed everything up like it's supposed to
xdotool key ctrl+f; s0
xdotool type --delay 50 'Send Now'; s0
xdotool key Escape; s0
xdotool key Return; s1;

until echo $sendconfirm | grep -Fq "Your email"; do
   s0; xdotool key ctrl+a; s0
   xdotool key ctrl+c; s0
   sendconfirm="$(clipit -c)"
done

# it should have sent the email...but for now lets just go to the dashboard

xdotool key ctrl+f; s0
xdotool type --delay 50 'Dashboard'; s0
xdotool key Escape; s0
xdotool key Return; s3
xdotool key ctrl+w; s0

while true; do
    ~/marketleaderbashscript/leads2.sh
done

exit 
